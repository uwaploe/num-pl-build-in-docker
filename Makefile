
## Optional parameters set via env arg
VERBOSE ?=

## Volumes used by all Docker versions
HOME_IN_DOCKER=/home/ros
DOCKER_WORKING_ROOT=${HOME_IN_DOCKER}/workspace

OpenCV_DIR = /usr/share/OpenCV/
CATKIN_ARGS ?=
DOCKER_RUN_ARGS=--rm -t -w ${DOCKER_WORKING_ROOT} \
								-e BUILD_WORKSPACE=${DOCKER_WORKING_ROOT} \
								-e OpenCV_DIR=${OpenCV_DIR}

ifneq ($(VERBOSE),)
	CATKIN_ARGS += --verbose
	DOCKER_RUN_ARGS += -e VERBOSE=1
endif


default: help

help:
	@echo "make init         Initialize the src/ directory with the repositories described in lsd-slam-ros-repos"
	@echo "make status       Run \"vcs status\""
	@echo "make pull         Run \"vcs pull\""
	@echo "make config       Update ROS deps and do catkin configuration"
	@echo ""
	@echo "make build        Run \"catkin build\""
	@echo "make test         Run \"catkin run_tests\""
	@echo "make clean        Run \"catkin clean\""
	@echo ""
	@echo "make bash         Run bash shall in Docker image"
	@echo ""
	@echo "make image        Build Docker build image"


##== ==

IMAGE_TAG=bionic-melodic
BUILD_IMAGE=amarburg/lsd-slam-ros:${IMAGE_TAG}

WORKING_SRCS=${PWD}/src
WORKING_WORKSPACE=${PWD}/${IMAGE_TAG}-workspace
WORKING_VOLUMES= -v "${PWD}/dot-ros:${HOME_IN_DOCKER}/.ros" \
								-v "${WORKING_WORKSPACE}:${DOCKER_WORKING_ROOT}" \
								-v "${PWD}/src:${DOCKER_WORKING_ROOT}/src" \
								-v "${PWD}/scripts:${DOCKER_WORKING_ROOT}/scripts" \
								-v "${PWD}/ssh:${HOME_IN_DOCKER}/.ssh"


# "Default" docker run command
DOCKER_RUN=docker run ${DOCKER_RUN_ARGS} ${WORKING_VOLUMES} ${BUILD_IMAGE}


init: working_dirs
	${DOCKER_RUN} scripts/vcs_wrapper.sh init

pull: working_dirs
	${DOCKER_RUN} scripts/vcs_wrapper.sh pull

config: working_dirs
	${DOCKER_RUN} scripts/vcs_wrapper.sh config

status: working_dirs
	${DOCKER_RUN} scripts/vcs_wrapper.sh status

build: working_dirs
	${DOCKER_RUN} catkin build ${CATKIN_ARGS}

## Test both with and without debugging output
test: working_dirs test_debug_pl test_non_debug_pl

test_debug_pl:
	docker run -e __DEBUG_PL=1 ${DOCKER_RUN_ARGS} ${WORKING_VOLUMES} ${BUILD_IMAGE} catkin build ${CATKIN_ARGS} --catkin-make-args run_tests && \
	for d in ${WORKING_SRCS}/; do  \
		${DOCKER_RUN}  catkin_test_results "build/$d"; \
	done

test_non_debug_pl:
	${DOCKER_RUN} catkin build ${CATKIN_ARGS} --catkin-make-args run_tests && \
	for d in ${WORKING_SRCS}/; do  \
		${DOCKER_RUN}  catkin_test_results "build/$d"; \
	done

clean: working_dirs
	${DOCKER_RUN} catkin clean --yes

bash: working_dirs
	docker run ${DOCKER_RUN_ARGS} ${WORKING_VOLUMES} --rm -it ${BUILD_IMAGE} bash

## As bash but with additional privileges
gdb_test: working_dirs
	docker run --cap-add=SYS_PTRACE --security-opt seccomp=unconfined ${DOCKER_RUN_ARGS} ${WORKING_VOLUMES} --rm -it ${BUILD_IMAGE} gdb

working_dirs: ${WORKING_WORKSPACE} ${WORKING_SRCS}

${WORKING_WORKSPACE}:
	mkdir -p $@/logs $@/scripts $@/src $@/install $@/dot-ros

${WORKING_SRCS}:
	mkdir -p $@

##== ==

image:
		docker build -t ${BUILD_IMAGE} docker/build-image/${IMAGE_TAG}/



.PHONY:  init pull config status build clean bash image working_dirs \
				test_debug_pl test_non_debug_pl
