#!/bin/bash

BUILD_WORKSPACE=${BUILD_WORKSPACE:-/home/ros/workspace}

if [ ! -f "$HOME/.ssh/id_rsa" ]; then
  echo "Need to provide an ssh keypair in ~root/.ssh with access to the localization_repos repo"
  exit 1
fi

if [ ! -n "$(grep "^gitlab.com " ~/.ssh/known_hosts)" ]; then
  ssh-keyscan gitlab.com  >> ~/.ssh/known_hosts 2>/dev/null;
fi
if [ ! -n "$(grep "^github.com " ~/.ssh/known_hosts)" ]; then
  ssh-keyscan github.com  >> ~/.ssh/known_hosts 2>/dev/null;
fi
if [ ! -n "$(grep "^bitbucket.org " ~/.ssh/known_hosts)" ]; then
  ssh-keyscan bitbucket.org  >> ~/.ssh/known_hosts 2>/dev/null;
fi

cd ${BUILD_WORKSPACE}/src/

if [ ! -d ${BUILD_WORKSPACE}/src/localization_repos ]; then
  git clone git@gitlab.com:apl-ocean-engineering/numurus_oot/num-pl-repos.git -b master
fi

subcommand=$1
case $subcommand in
    "init" )
        vcs import < num-pl-repos/num-pl.repos
        ;&
    "dep" )
        # use rosdep to install dependencies
        rosdep update
        . /opt/ros/melodic/setup.sh
        cd ${BUILD_WORKSPACE}
        rosdep install --from-paths src --ignore-src -r -y
        ;;
    "pull" )
        echo "Pulling...."
        vcs pull
        ;;
    "status" )
        vcs status
        ;;
    "config" )
        cd ${BUILD_WORKSPACE}
        catkin config --install --extend /opt/ros/melodic
        catkin config --cmake-args -DCMAKE_BUILD_TYPE=RelWithDebInfo
        ;;
    *)
        shift
        sub_${subcommand} $@
        if [ $? = 127 ]; then
            echo "Error: '$subcommand' is not a known subcommand." >&2
            echo "       Run '$ProgName --help' for a list of known subcommands." >&2
            exit 1
        fi
        ;;
esac
